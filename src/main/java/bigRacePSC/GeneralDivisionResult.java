package bigRacePSC;

import java.util.Arrays;
import java.util.Comparator;

public class GeneralDivisionResult {
	
	private Integer position = null;
	private Manager manager = null;
	private Integer[] sumup = {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null};
	private Integer total = null;
	

	public GeneralDivisionResult(final Manager m) {
		manager = m;
	}

	
	public void updateTotalPoints() {
		Integer result = 0;
		for (int i = 0; i < sumup.length && sumup[i] != null; ++i) {
			result += sumup[i];
		}
		
		total = result;
	}
	
	
	public static Comparator<GeneralDivisionResult> DIVISION_GENERAL_ID_COMPARATOR = new Comparator<GeneralDivisionResult> () {

		@Override
		public int compare(GeneralDivisionResult o1, GeneralDivisionResult o2) {
			try {
				return o1.getManager().getId() - o2.getManager().getId();
			} catch (NullPointerException e) {
				System.err.println("ERREUR : NullPointer sur les ID de manager de deux objets GeneralDivisionResult :\n" + e.getMessage());
				return 0;
			}
		}
	};
	
	
	public static Comparator<GeneralDivisionResult> DIVISION_GENERAL_POINTS_COMPARATOR = new Comparator<GeneralDivisionResult>() {

		@Override
		public int compare(GeneralDivisionResult o1, GeneralDivisionResult o2) {
			try {
				if (o1.getManager().getDivision() < o2.getManager().getDivision()) {
					return -1;
				} else if (o1.getManager().getDivision() > o2.getManager().getDivision()) {
					return 1;
				} else {
					return o2.getTotal() - o1.getTotal();
				}
			} catch (NullPointerException e) {
				System.err.println("ERREUR Null Pointer sur la comparaison de deux résultats globaux par divisions :\n" + e.getMessage());
				return 0;
			}
		}
	};
	

	public Integer getPosition() {
		return position;
	}


	public void setPosition(Integer position) {
		this.position = position;
	}


	public Manager getManager() {
		return manager;
	}


	public void setManager(Manager manager) {
		this.manager = manager;
	}


	public Integer[] getSumup() {
		return sumup;
	}


	public void setSumup(Integer[] sumup) {
		this.sumup = sumup;
	}


	public Integer getTotal() {
		return total;
	}


	public void setTotal(Integer total) {
		this.total = total;
	}


	@Override
	public String toString() {
		return "GeneralDivisionResult [position=" + position + ", manager=" + manager + ", sumup="
				+ Arrays.toString(sumup) + ", total=" + total + "]";
	}
}
