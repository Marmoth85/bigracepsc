package bigRacePSC;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class Classifier {
	
	private Map<Manager, Race> gpResults;
	private List<BigRaceResult> bigRaceResults;
	private List<GeneralBigRaceResult> generalBigRaceResults;
	private List<DivisionResult> divisionResults;
	private List<GeneralDivisionResult> generalDivisionResults;
	private Map<Integer, RaceTime> storedBigRaceTime;
	private Map<Integer, Integer[]> storedDivisionPoints;
	private Integer gp;

	
	public Classifier() {
		bigRaceResults = new ArrayList<>();
		divisionResults = new ArrayList<>();
		generalBigRaceResults = new ArrayList<>();
		generalDivisionResults = new ArrayList<>();
		gpResults = null;
		storedDivisionPoints = null;
		storedBigRaceTime = null;
		gp = null;
	}
	
	
	public Classifier(Map<Manager, Race> results, final int gp) {
		this();
		gpResults = results;
		this.gp = gp;
	}
	
	
	public void computeBigRaceResults() throws Exception {
		
		if (gpResults == null) {
			throw new Exception("Erreur : la Map des résultats est vide!");
		}
		
		final Set<Map.Entry<Manager, Race>> perf = gpResults.entrySet();
		final Iterator<Map.Entry<Manager, Race>> iter = perf.iterator();
		
		while(iter.hasNext()) {
			final Map.Entry<Manager, Race> paire = iter.next();
			final Manager manager = paire.getKey();
			final Race race = paire.getValue();
			
			final BigRaceResult br = new BigRaceResult(manager, race);
			final DivisionResult dr = new DivisionResult(manager, race, gp);
			final GeneralDivisionResult gdr = new GeneralDivisionResult(manager);
			GeneralBigRaceResult gbr = null;
			
			if (gp == 1) {
				gbr = new GeneralBigRaceResult(manager, race, null);
			} else {
				gbr = new GeneralBigRaceResult(manager, race, storedBigRaceTime.get(manager.getId()));
				gdr.setSumup(storedDivisionPoints.get(manager.getId()));
			}
			gbr.addRaceTimeToGlobalTime();
			
			bigRaceResults.add(br);
			divisionResults.add(dr);
			generalBigRaceResults.add(gbr);
			generalDivisionResults.add(gdr);
		}
		// Big Race du gp courant
		Collections.sort(bigRaceResults, BigRaceResult.BIG_RACE_GP_TIME_COMPARATOR);
		calculateBigRaceGapTimes();
		// Classements par divisions
		Collections.sort(divisionResults, DivisionResult.DIVISION_GP_TIME_COMPARATOR);
		calculateDivisionPoints();
		// Classement global PSC Big Race
		Collections.sort(generalBigRaceResults, GeneralBigRaceResult.BIG_RACE_GENERAL_TIME_COMPARATOR);
		calculateGeneralBigRaceGapTimes();
		// Mise à jour infos du général par division
		updateGeneralDivision();
		// Classement général par division
		Collections.sort(generalDivisionResults, GeneralDivisionResult.DIVISION_GENERAL_POINTS_COMPARATOR);
		calculateGeneralDivisionPositions();
	}
	
	
	private void calculateBigRaceGapTimes() {
		int i = 1;
		
		final BigRaceResult leaderResult = bigRaceResults.get(0);
		BigRaceResult previousResult = bigRaceResults.get(0);
		leaderResult.setPosition(1);
		
		for (BigRaceResult bigRaceResult : bigRaceResults) {
				
			if (BigRaceResult.BIG_RACE_GP_TIME_COMPARATOR.compare(previousResult, bigRaceResult) == 0) {
				bigRaceResult.setPosition(previousResult.getPosition());
			} else {
				bigRaceResult.setPosition(i);
			}
			
			final Double gapPrevious = bigRaceResult.getRaceTime().getNumericTime() - previousResult.getRaceTime().getNumericTime();
			final RaceTime rtPrevious = new RaceTime();
			rtPrevious.setNumericTime(gapPrevious);
			rtPrevious.calculateStringTimeFromNumeric();
			bigRaceResult.setGapPrevious(rtPrevious);
			
			final Double gapLeader = bigRaceResult.getRaceTime().getNumericTime() - leaderResult.getRaceTime().getNumericTime();
			final RaceTime rtLeader = new RaceTime();
			rtLeader.setNumericTime(gapLeader);
			rtLeader.calculateStringTimeFromNumeric();
			bigRaceResult.setGapLeader(rtLeader);
			
			previousResult = bigRaceResult;
			
			i++;
		}
	}
	
	
	private void calculateDivisionPoints() {
		int i = 1;
		// TODO Importer barème des points en fonction de la division via fichier de config
		// En attendant cette fonctionnalité, les points sont hardcodés.
		int[] elites = {};
		int[] masters = {4,2,1};
		int[] pros = {50,40,35,30,27,24,21,18,16,14,12,10,8,6,5,4,3,2,1};
		int[] amateurs = {10,7,5,3,2,1};
		int[] rookies = {1};
		List<int[]> rules = new ArrayList<>();
		rules.add(elites);
		rules.add(masters);
		rules.add(pros);
		rules.add(amateurs);
		rules.add(rookies);
		
		DivisionResult previous = divisionResults.get(0);
		previous.setPosition(1);
		
		for (DivisionResult divisionResult : divisionResults) {
			if (DivisionResult.DIVISION_GP_TIME_COMPARATOR.compare(previous, divisionResult) == 0) {
				divisionResult.setPosition(previous.getPosition());
			} else {
				if (previous.getManager().getDivision() != divisionResult.getManager().getDivision()) {
					i = 1;
					previous = divisionResult;
				}
				divisionResult.setPosition(i);
			}
			divisionResult.setPoints(rules.get(divisionResult.getManager().getDivision() - 1)[divisionResult.getPosition() - 1]);
			
			// Rajout d'une règle pour ne pas scorer de points en cas de DNS
			final double racetime = divisionResult.getRaceTime().getNumericTime().doubleValue();
			if (racetime == 14400d) {
				divisionResult.setPoints(0);
			}
			
			previous = divisionResult;
			i++;
		}
	}
	
	
	private void calculateGeneralBigRaceGapTimes() {
		int i = 1;
		
		final GeneralBigRaceResult leaderResult = generalBigRaceResults.get(0);
		GeneralBigRaceResult previousResult = generalBigRaceResults.get(0);
		leaderResult.setPosition(1);
		
		for (GeneralBigRaceResult generalBigRaceResult : generalBigRaceResults) {
				
			if (GeneralBigRaceResult.BIG_RACE_GENERAL_TIME_COMPARATOR.compare(previousResult, generalBigRaceResult) == 0) {
				generalBigRaceResult.setPosition(previousResult.getPosition());
			} else {
				generalBigRaceResult.setPosition(i);
			}
			
			final Double gapPrevious = generalBigRaceResult.getGlobalTime().getNumericTime() - previousResult.getGlobalTime().getNumericTime();
			final RaceTime rtPrevious = new RaceTime();
			rtPrevious.setNumericTime(gapPrevious);
			rtPrevious.calculateStringTimeFromNumeric();
			generalBigRaceResult.setGapPrevious(rtPrevious);
			
			final Double gapLeader = generalBigRaceResult.getGlobalTime().getNumericTime() - leaderResult.getGlobalTime().getNumericTime();
			final RaceTime rtLeader = new RaceTime();
			rtLeader.setNumericTime(gapLeader);
			rtLeader.calculateStringTimeFromNumeric();
			generalBigRaceResult.setGapLeader(rtLeader);
			
			previousResult = generalBigRaceResult;
			
			i++;
		}
	}
	
	
	private void updateGeneralDivision() throws Exception {
		
		Collections.sort(divisionResults, DivisionResult.DIVISION_GP_ID_COMPARATOR);
		Collections.sort(generalDivisionResults, GeneralDivisionResult.DIVISION_GENERAL_ID_COMPARATOR);
		
		if (divisionResults == null || generalDivisionResults == null) {
			throw new Exception("DivisionResults ou GeneralDivisionResults n'ont pas été construits correctement!");
		}
		
		if (divisionResults.size() != generalDivisionResults.size()) {
			throw new Exception("DivisionResults et GeneralDivisionResults n'ont pas le même nombre d'éléments!");
		}
		
		int number = divisionResults.size();
		for (int i = 0; i < number; ++i) {
			
			final DivisionResult gpResult = divisionResults.get(i);
			final GeneralDivisionResult generalResult = generalDivisionResults.get(i);
			
			Integer[] sumup = generalResult.getSumup();
			sumup[gpResult.getGp() - 1] = gpResult.getPoints();
			generalResult.setSumup(sumup);
			
			generalResult.updateTotalPoints();
		}
		Collections.sort(divisionResults, DivisionResult.DIVISION_GP_TIME_COMPARATOR);
	}

	
	public void calculateGeneralDivisionPositions() {
		int i = 1;
		GeneralDivisionResult previous = generalDivisionResults.get(0);
		previous.setPosition(1);
		
		for (GeneralDivisionResult general : generalDivisionResults) {
			
			if (GeneralDivisionResult.DIVISION_GENERAL_POINTS_COMPARATOR.compare(previous, general) == 0) {
				general.setPosition(previous.getPosition());
			} else {
				if (previous.getManager().getDivision() != general.getManager().getDivision()) {
					i = 1;
					previous = general;
				}
				general.setPosition(i);
			}
			
			previous = general;
			i++;
		}
	}
	
	
	public void createDirectoryIfNotExists(final String nomFichier) {
		final File f = new File(nomFichier);
		final File parent = f.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}
	}
	
	
	private void exportDivisionRaceResult(final BufferedWriter bw) throws IOException {
		bw.write("division-race;");
		String line = "";
		
		for (final DivisionResult item : divisionResults) {
			
			bw.newLine();
			
			line = "" + item.getPosition() + ";";
			line += item.getManager().getId() + ";";
			line += item.getManager().getGroup() + ";";
			line += item.getRaceTime().getStringTime() + ";";
			line += item.getClassification() + ";";
			line += item.getBestLap().getStringTime() + ";";
			line += item.getPoints() + ";";
			
			bw.write(line);
		}
	}
	
	
	private void exportDivisionGeneralResults(final BufferedWriter bw) throws IOException {
		bw.newLine();
		bw.write("division-general;");
		String line = "";
		
		for (final GeneralDivisionResult item : generalDivisionResults) {
			bw.newLine();
			
			line = "" + item.getPosition() + ";";
			line += item.getManager().getId() + ";";
			Integer[] pts = item.getSumup();
			for (int i = 0; i < pts.length; ++i) {
				if (pts[i] != null) {
					line += pts[i] + ";";
				} else {
					line += ";";
				}
			}
			line += item.getTotal() + ";";
			
			bw.write(line);
		}
	}
	
	
	private void exportBigRaceResult(final BufferedWriter bw) throws IOException {
		bw.newLine();
		bw.write("big race;");
		String line = "";
		
		for (final BigRaceResult item : bigRaceResults) {
			bw.newLine();
			
			line = "" + item.getPosition() + ";";
			line += item.getManager().getId() + ";";
			line += item.getManager().getGroup() + ";";
			line += item.getRaceTime().getStringTime() + ";";
			line += item.getGapPrevious().getStringTime() + ";";
			line += item.getGapLeader().getStringTime() + ";";
			
			bw.write(line);
		}
	}
	
	
	private void exportBigRaceGeneralResults(final BufferedWriter bw) throws IOException {
		bw.newLine();
		bw.write("big race - general;");
		String line = "";
		
		for (final GeneralBigRaceResult item : generalBigRaceResults) {
			bw.newLine();
			
			line = "" + item.getPosition() + ";";
			line += item.getManager().getId() + ";";
			line += item.getManager().getGroup() + ";";
			line += item.getGlobalTime().getStringTime() + ";";
			line += item.getGapPrevious().getStringTime() + ";";
			line += item.getGapLeader().getStringTime() + ";";
			
			bw.write(line);
		}
	}
	
	
	public void exportSumUpCSV(final String csvFile) throws IOException {
		createDirectoryIfNotExists(csvFile);
		
		BufferedWriter bw = null;
		
		try {
			bw = new BufferedWriter(new FileWriter(csvFile, false));
			
			exportDivisionRaceResult(bw);
			exportDivisionGeneralResults(bw);
			exportBigRaceResult(bw);
			exportBigRaceGeneralResults(bw);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}
	
	
	private void importDivisionGeneralData(final BufferedReader br) throws IOException {
		storedDivisionPoints = new HashMap<>();
		String line = "";
		
		while ((line = br.readLine()) != null && line.split(";") != null && line.split(";").length != 1) {
			String[] splittedLine = line.split(";");
			
			final Integer id = Integer.valueOf(splittedLine[1]);
			final Integer[] pts = new Integer[17];
			for (int i = 0; i < pts.length; ++i) {
				if (StringUtils.isBlank(splittedLine[i + 2])) {
					pts[i] = null;
				} else {
					pts[i] = Integer.valueOf(splittedLine[i + 2]);
				}
			}
			storedDivisionPoints.put(id, pts);
		}
	}
	
	
	private void importBigRaceGeneralData(final BufferedReader br) throws Exception {
		storedBigRaceTime = new HashMap<>();
		String line = "";
		
		while ((line = br.readLine()) != null && line.split(";") != null && line.split(";").length != 1) {
			String[] splittedLine = line.split(";");
			
			final Integer id = Integer.valueOf(splittedLine[1]);
			final String raceTime = splittedLine[3];
			final RaceTime rt = new RaceTime();
			rt.setStringTime(raceTime);
			rt.calculateNumericTimeFromString(true);
			
			storedBigRaceTime.put(id, rt);
		}
	}
	
	
	public void importPreviousSumUpCSV(final String fileName) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line;
		
		while ((line = br.readLine()) != null) {
			String[] splittedLine = line.split(";");
			if ("division-general".equals(splittedLine[0])) {
				importDivisionGeneralData(br);
			}
			if ("big race - general".equals(splittedLine[0])) {
				importBigRaceGeneralData(br);
			}
		}
		br.close();
	}
	
	
	private void generateHeaderFile(final BufferedWriter bw) throws IOException {
		String content = "<!DOCTYPE html>\r\n" + 
				"<html lang=\"fr\">\r\n" + 
				"<head>\r\n" + 
				"    <meta charset=\"UTF-8\">\r\n" + 
				"    <meta name=\"viewport\" content=\"width=<device-width>, initial-scale=1.0\">\r\n" + 
				"    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n" + 
				"    <title>Classements</title>\r\n" + 
				"</head>\r\n" + 
				"<style>\r\n" + 
				"    .cMonTableauRule {\r\n" + 
				"        width: 70%;\r\n" + 
				"        margin-left: 15%;\r\n" + 
				"        margin-right: 15%;\r\n" + 
				"        border-collapse: collapse;\r\n" + 
				"        border-spacing: 0px;\r\n" + 
				"        border-top: 4px solid #808080;\r\n" + 
				"        border-bottom: 4px solid #808080;\r\n" + 
				"        border-left: 4px solid #808080;\r\n" + 
				"        border-right: 4px solid #808080;\r\n" + 
				"        font-family: Arial, Helvetica, sans-serif;\r\n" + 
				"        font-size: 0.8em;" + 
				"    }\r\n" + 
				"    .cMonTableauRule th {\r\n" + 
				"        border-top: 4px solid #808080;\r\n" + 
				"        border-bottom: 4px solid #808080;\r\n" + 
				"        border-right: 4px solid #808080;\r\n" + 
				"    }\r\n" + 
				"    .cMonTableauRule td {\r\n" + 
				"        margin-left: 10px;\r\n" + 
				"        border-right: 4px solid #808080;\r\n" + 
				"    }\r\n" + 
				"    .centeredCell {\r\n" + 
				"        text-align: center;\r\n" + 
				"        padding-left: 0px;\r\n" + 
				"    }\r\n" + 
				"    .leftCell {\r\n" + 
				"        text-align: left;\r\n" + 
				"        padding-left: 10px;\r\n" + 
				"    }\r\n" + 
				"    .gap {\r\n" + 
				"        font-style: italic;\r\n" + 
				"    }\r\n" + 
				"    .important {\r\n" + 
				"        font-weight: bold;\r\n" + 
				"    }\r\n" + 
				"    h1 {\r\n" + 
				"        text-align: center;\r\n" + 
				"        font-family: Arial, Helvetica, sans-serif;\r\n" + 
				"    }\r\n" + 
				"    .PSC-Green-Cobra {\r\n" + 
				"        color: green;\r\n" + 
				"    }\r\n" + 
				"    .PSC-Red-Viper {\r\n" + 
				"        color : red;\r\n" + 
				"    }\r\n" + 
				"    .PSC-Orange-Python {\r\n" + 
				"        color : orange;\r\n" + 
				"    }\r\n" + 
				"</style>\r\n" + 
				"<body>\r\n";
		bw.write(content);
	}
	
	
	private String buildEmptyLine(int cells) {
		String result = "";
		
		result += "        <tr>\r\n";
		for (int i = 0; i < cells; ++i) {
			result += "            <td>&nbsp;</td>\r\n";
		}
		result += "        </tr>\r\n";
		
		return result;
	}
	
	
	private void generateContentDivisionTable(final BufferedWriter bw) throws IOException {
		int currentDiv = -1;
		
		String[] divisionName = {"Elite", "Master", "Pro", "Amateur", "Rookie"};
		
		for (final DivisionResult item : divisionResults) {
			String content = "";
			
			if (item.getManager().getDivision() != currentDiv) {
				if (currentDiv != -1) {
					content += buildEmptyLine(7);
				}
				content += "        <tr>\r\n" + 
						"            <th width=\"10%\">Position</th>\r\n" + 
						"            <th width=\"25%\">" + divisionName[item.getManager().getDivision() - 1]+"</th>\r\n" + 
						"            <th width=\"12.5%\">Groupe</th>\r\n" + 
						"            <th width=\"17.5%\">Temps</th>\r\n" + 
						"            <th width=\"12.5%\">Classement</th>\r\n" + 
						"            <th width=\"12.5%\">Meilleur tour</th>\r\n" + 
						"            <th width=\"10%\">Points</th>\r\n" + 
						"        </tr>\r\n";
				content += buildEmptyLine(7);
				bw.write(content);
				currentDiv = item.getManager().getDivision();
			}
			
			content = "        <tr>\r\n" + 
					"            <td class=\"centeredCell important\">" + item.getPosition() + "</td>\r\n" + 
					"            <td class=\"leftCell " + item.getManager().getTeam().replace(" ", "-") + " important\">" + item.getManager().getName() + "</td>\r\n" + 
					"            <td class=\"leftCell\">" + item.getManager().getGroup() + "</td>\r\n" + 
					"            <td class=\"centeredCell\">" + item.getRaceTime().getStringTime() + "</td>\r\n" + 
					"            <td class=\"centeredCell\">" + item.getClassification() + "</td>\r\n" + 
					"            <td class=\"centeredCell\">" + item.getBestLap().getStringTime() +"</td>\r\n" + 
					"            <td class=\"centeredCell important\">" + item.getPoints() + "</td>\r\n" + 
					"        </tr>\r\n";
			bw.write(content);
		}
		bw.write(buildEmptyLine(7));
	}
	
	
	private String buildHeaderGeneralDivisionTable(final String divisionName) {
		
		String result = "        <tr>\r\n" + 
				"            <th width=\"8%\">Position</th>\r\n" + 
				"            <th width=\"25%\">" + divisionName + "</th>\r\n" + 
				"            <th width=\"3.5%\">GP1</th>\r\n" + 
				"            <th width=\"3.5%\">GP2</th>\r\n" + 
				"            <th width=\"3.5%\">GP3</th>\r\n" + 
				"            <th width=\"3.5%\">GP4</th>\r\n" + 
				"            <th width=\"3.5%\">GP5</th>\r\n" + 
				"            <th width=\"3.5%\">GP6</th>\r\n" + 
				"            <th width=\"3.5%\">GP7</th>\r\n" + 
				"            <th width=\"3.5%\">GP8</th>\r\n" + 
				"            <th width=\"3.5%\">GP9</th>\r\n" + 
				"            <th width=\"3.5%\">GP10</th>\r\n" + 
				"            <th width=\"3.5%\">GP11</th>\r\n" + 
				"            <th width=\"3.5%\">GP12</th>\r\n" + 
				"            <th width=\"3.5%\">GP13</th>\r\n" + 
				"            <th width=\"3.5%\">GP14</th>\r\n" + 
				"            <th width=\"3.5%\">GP15</th>\r\n" + 
				"            <th width=\"3.5%\">GP16</th>\r\n" + 
				"            <th width=\"3.5%\">GP17</th>\r\n" + 
				"            <th width=\"7.5%\">Total</th>\r\n" + 
				"        </tr>\r\n" + 
				"";
		
		return result;
	}
	
	
	private String buildLineGeneralDivisionTable(final GeneralDivisionResult item) {
		
		String content = "        <tr>\r\n" + 
				"            <td class=\"centeredCell important\">" + item.getPosition() + "</td>\r\n" + 
				"            <td class=\"leftCell " + item.getManager().getTeam().replace(" ", "-") + " important\">" + item.getManager().getName() + "</td>\r\n" + 
				"";
		
		for (int i = 0; i < item.getSumup().length; ++i) {
			final Integer pts = item.getSumup()[i];
			content += "            <td class=\"centeredCell\">" + (pts == null ? "" : pts) + "</td>\r\n";
		}
		
		content += "            <td class=\"centeredCell important\">" + item.getTotal() + "</td>\r\n" + 
				"        </tr>\r\n" + 
				"";
		
		return content;
	}
	
	
	private void generateContentGeneralDivisionTable(final BufferedWriter bw) throws IOException {
		int currentDiv = -1;
		
		String[] divisionName = {"Elite", "Master", "Pro", "Amateur", "Rookie"};
		
		for (final GeneralDivisionResult item : generalDivisionResults) {
			
			String content = "";
			if (item.getManager().getDivision() != currentDiv) {
				if (currentDiv != -1) {
					content += buildEmptyLine(20);
				}
				content += buildHeaderGeneralDivisionTable(divisionName[item.getManager().getDivision() - 1]);
				content += buildEmptyLine(20);
				bw.write(content);
				currentDiv = item.getManager().getDivision();
			}
			
			content = buildLineGeneralDivisionTable(item);
			bw.write(content);
		}
		bw.write(buildEmptyLine(20));
	}
	
	
	private String buildHeaderBigRaceTable() {
		
		String content = "        <tr>\r\n" + 
				"            <th width=\"10%\">Position</th>\r\n" + 
				"            <th width=\"25%\">Nom manager</th>\r\n" + 
				"            <th width=\"12.5%\">Groupe</th>\r\n" + 
				"            <th width=\"20%\">Temps de course</th>\r\n" + 
				"            <th width=\"15%\">Ecart / précédent</th>\r\n" + 
				"            <th width=\"17.5%\">Ecart / leader</th>\r\n" + 
				"        </tr>\r\n";
		content += buildEmptyLine(6);
		return content;
	}
	
	
	private String buildBigRaceLine(final BigRaceResult item) {
		
		String content = "        <tr>\r\n" + 
				"            <td class=\"centeredCell important\">" + item.getPosition() + "</td>\r\n" + 
				"            <td class=\"leftCell important " + item.getManager().getTeam().replace(" ",  "-") + "\">" + item.getManager().getName() + "</td>\r\n" + 
				"            <td class=\"leftCell\">" + item.getManager().getGroup() + "</td>\r\n" + 
				"            <td class=\"centeredCell important\">" + item.getRaceTime().getStringTime() + "</td>\r\n" + 
				"            <td class=\"centeredCell gap\">" + item.getGapPrevious().getStringTime() + "</td>\r\n" + 
				"            <td class=\"centeredCell gap\">" + item.getGapLeader().getStringTime() + "</td>\r\n" + 
				"        </tr>\r\n";
		return content;
	}
	
	
	private void generateBigRaceTable(final BufferedWriter bw) throws IOException {
		
		String content = buildHeaderBigRaceTable();
		
		for (final BigRaceResult item : bigRaceResults) {
			content += buildBigRaceLine(item);
		}
		
		content += buildEmptyLine(6);
		
		bw.write(content);
	}
	
	
	private String buildGeneralBigRaceLine(final GeneralBigRaceResult item) {
		
		String content = "\r\n" + 
				"        <tr>\r\n" + 
				"            <td class=\"centeredCell important\">" + item.getPosition() + "</td>\r\n" + 
				"            <td class=\"leftCell important " + item.getManager().getTeam().replace(" ", "-") + "\">" + item.getManager().getName() + "</td>\r\n" + 
				"            <td class=\"leftCell\">" + item.getManager().getGroup() + "</td>\r\n" + 
				"            <td class=\"centeredCell important\">" + item.getGlobalTime().getStringTime() + "</td>\r\n" + 
				"            <td class=\"centeredCell gap\">" + item.getGapPrevious().getStringTime() + "</td>\r\n" + 
				"            <td class=\"centeredCell gap\">" + item.getGapLeader().getStringTime() + "</td>\r\n" + 
				"        </tr>\r\n";
		
		return content;
	}
	
	
	private void generateGenerateBigRaceTable(final BufferedWriter bw) throws IOException {
		
		String content = buildHeaderBigRaceTable();
		
		for (final GeneralBigRaceResult item : generalBigRaceResults) {
			content += buildGeneralBigRaceLine(item);
		}
		
		content += buildEmptyLine(6);
		
		bw.write(content);
	}
	
	
	public void generateHTML(final String fileName) throws IOException {
		
		createDirectoryIfNotExists(fileName);
		
		final BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, false));
		
		generateHeaderFile(bw);
		
		bw.write("    <h1>Classement par divisions</h1>\r\n" + 
				"\r\n" + 
				"    <table class=\"cMonTableauRule\" width=\"100%\">\r\n" + 
				"");
		generateContentDivisionTable(bw);
		
		bw.write("\r\n" + 
				"    </table>\r\n" + 
				"    \r\n" + 
				"	\r\n" + 
				"    <h1>Classement général par divisions</h1>\r\n" + 
				"\r\n" + 
				"    <table class=\"cMonTableauRule\">\r\n" + 
				"");
		generateContentGeneralDivisionTable(bw);
		
		bw.write("\r\n" + 
				"    </table>\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"    <h1>Classement PSC Big Race</h1>\r\n" + 
				"\r\n" + 
				"    <table class=\"cMonTableauRule\">\r\n" + 
				"");
		generateBigRaceTable(bw);
		
		bw.write("    </table>\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"    <h1>Classement général de la PSC Big Race</h1>\r\n" + 
				"\r\n" + 
				"    <table class=\"cMonTableauRule\">\r\n" + 
				"");
		generateGenerateBigRaceTable(bw);
		
		bw.write("    </table>\r\n" + 
				"</body>\r\n" + 
				"\r\n" + 
				"</html>");
		
		bw.close();
	}

	
	public List<BigRaceResult> getBigRaceResults() {
		return bigRaceResults;
	}


	public List<GeneralBigRaceResult> getGeneralBigRaceResults() {
		return generalBigRaceResults;
	}


	public List<DivisionResult> getDivisionResults() {
		return divisionResults;
	}


	public List<GeneralDivisionResult> getGeneralDivisionResults() {
		return generalDivisionResults;
	}


	public Map<Manager, Race> getGpResults() {
		return gpResults;
	}


	public void setGpResults(Map<Manager, Race> gpResults) {
		this.gpResults = gpResults;
	}
}
