package bigRacePSC;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class RaceURL {
	
	private List<Manager> managers;
	private String directoryPath;
	
	private Integer saison = null;
	private Integer gp = null;
	private String outputRaceFilePathName = null;
	private String outputSumUpFilePathName = null;
	private String previousSumUpFilePathName = null;
	private String htmlFilePath = null;
	
	private Scanner scanner = null;
	
	
	public static void main(String[] args) throws Exception {
		final RaceURL ru = new RaceURL();
		ru.loadConfigFile();
		ru.processText();
	}
	
	
	private void loadConfigFile() throws Exception {
		final ConfigLoader configLoader = new ConfigLoader();
		configLoader.readConfigFile();
		final Map<String, String> parameters = configLoader.getParameters();
		directoryPath = parameters.get(ConfigLoader.PARAM_NAME_DIRECTORY_PATH);
		final String managerFilePath = parameters.get(ConfigLoader.PARAM_NAME_PARTICIPANTS_FILE_PATH);
		managers = configLoader.getManagers(managerFilePath);
	}
	
	
	private boolean testLastRace() {
		if (gp == 1) {
			return true;
		}
		File f = new File(previousSumUpFilePathName);
		if (f.exists() && !f.isDirectory()) {
			return true;
		} else {
			return false;
		}
	}
	
	
	private void process() throws Exception {
		if (testInputs()) {
			
			if (!testLastRace()) {
				System.err.println("Vous devez d'abord récupérer les données de grands prix antérieurs");
				return;
			}
			
			final Map<Manager, Race> lastGP = getRaceInfoForAllManagers(managers, saison, gp);
			final RaceExporter export = new RaceExporter(lastGP);
			export.ecrireFichierCSV(outputRaceFilePathName);
			
			final Classifier classifier = new Classifier(lastGP, gp);
			if (gp != 1) classifier.importPreviousSumUpCSV(previousSumUpFilePathName);
			classifier.computeBigRaceResults();
			classifier.exportSumUpCSV(outputSumUpFilePathName);
			classifier.generateHTML(htmlFilePath);
		} else {
			processText();
		}
	}
	
	
	private boolean testInputs() {
		if (saison != null && gp != null && StringUtils.isNotBlank(outputRaceFilePathName)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	private void buildOutputFilePathName() {
		if (saison != null && gp != null) {
			String raceFilePath = "";
			String sumUpFilePath = "";
			if (gp < 10) {
				raceFilePath = "0";
				sumUpFilePath = "0";
			}
			raceFilePath = directoryPath + "\\S" + String.valueOf(saison) + "\\gp" + raceFilePath;
			sumUpFilePath = directoryPath + "\\S" + String.valueOf(saison) + "\\sumup" + sumUpFilePath;
			outputRaceFilePathName = raceFilePath + String.valueOf(gp) + ".csv";
			outputSumUpFilePathName = sumUpFilePath + String.valueOf(gp) + ".csv";
			previousSumUpFilePathName = sumUpFilePath + (gp == 10 ? "0" : "") + String.valueOf(gp - 1)+ ".csv";
			htmlFilePath = sumUpFilePath + String.valueOf(gp) + ".html";
		} else {
			outputRaceFilePathName = null;
			outputSumUpFilePathName = null;
			previousSumUpFilePathName = null;
		}
	}
	
	
	private Integer getIntegerFromConsole(final String message, final Integer minValue, final Integer maxValue) {
		
		Integer result = null;
		boolean correctAnswer = false;
		
		while (!correctAnswer) {
			
			result = null;
			System.out.println(message);
			
			try {
				
				result = scanner.nextInt();
				
				if (minValue != null && result < minValue) {
					System.err.println("Votre réponse n'est pas correcte car inférieure au minimum attendu : " + minValue);
				}
				
				if (maxValue != null && result > maxValue) {
					System.err.println("Votre réponse n'est pas correcte car supérieure au maximum attendu : " + maxValue);
				}
				
				if ((minValue != null && minValue <= result && maxValue != null && result <= maxValue)
						|| (minValue != null && minValue <= result && maxValue == null)
						|| (minValue == null && maxValue != null && result <= maxValue)
						|| (minValue == null && maxValue == null)) {
					correctAnswer = true;
					scanner.nextLine();
				}
			}
			catch (InputMismatchException e) {
				System.err.println("Votre réponse n'est pas un nombre entier comme attendu : veuillez corriger votre réponse.\n");
				result = null;
				scanner.nextLine();
			}
		}
		
		return result;
	}
	
	
	private Integer getSaisonFromConsole() {
		
		final String message = "Veuillez saisir le numéro de la saison du gp souhaité : ";
		final Integer result = getIntegerFromConsole(message, 1, null);
		
		return result;
	}
	
	
	private Integer getGpFromConsole() {
		
		final String message = "Veuillez saison le numéro de GP de la saison choisie : ";
		final Integer result = getIntegerFromConsole(message, 1, 17);
		
		return result;
	}
	
	
	public void processText() throws Exception {
		
		loadConfigFile();
		if (scanner == null) {
			scanner = new Scanner(System.in);
			getParamFromConsole();
			process();
			scanner.close();
		} else {
			getParamFromConsole();
			process();
		}
	}


	private void getParamFromConsole() throws IOException {
		
		saison = getSaisonFromConsole();
		gp = getGpFromConsole();
		buildOutputFilePathName();
	}
	
	
	public Map<Manager, Race> getRaceInfoForAllManagers(final List<Manager> managers, final int saison, final int gp) throws Exception {
		final Map <Manager, Race> gpMap = new HashMap<>();
		
		for (final Manager manager : managers) {
			
			if (manager != null && manager.getId() != null) {
				
				System.out.println("Récupération des données du manager n°" + manager.getId() + ", à savoir " + manager.getName() + 
						(StringUtils.isBlank(manager.getTeam()) ? "..." : " de la team " + manager.getTeam() + "..."));
				final String raceUrl = "https://gpro.net/fr/RacingResume.asp?Season=" + saison + "&IDM=" + manager.getId();
				final Race race = getRaceInfoForOneManager(raceUrl, saison, gp);
				
				// Vérification de start crash. Si ça arrive, on prend temps Leader + 0h30mn.
				if (!race.getTempsCourse().equals("4h00:00.000") && race.getToursFinis() == 0) {
					
					System.out.println("START CRASH pour " + manager.getName() + " lors du GP!!!");
					final String classementUrl = "https://gpro.net/fr/RaceSummary.asp?group=" + 
							manager.getGroup().replace(" ", "%20") + "&Season=" + saison + "&Race=" + gp;
					final RaceTime leaderTime = getLeaderTime(classementUrl);
					final RaceTime managerTime = new RaceTime();
					managerTime.setNumericTime(leaderTime.getNumericTime() + 1800);
					managerTime.calculateStringTimeFromNumeric();
					race.setTempsCourse(managerTime.getStringTime());
				}
				
				gpMap.put(manager, race);
			}
		}
		return gpMap;
	}
	
	
	private RaceTime getLeaderTime(final String url) throws Exception {
		
		final RaceTime result = new RaceTime();
		final Connection conn = Jsoup.connect(url);
		conn.timeout(30000);
		
		final Document doc = conn.get();
		final String time = doc.select("table").get(0).select("tr").get(1).select("td").get(6).text();
		result.setStringTime(time);
		result.calculateNumericTimeFromString(true);
		
		return result;
	}


	public Race getRaceInfoForOneManager(final String url, final int saison, final int gp) throws IOException {
		
		final Connection connexion = Jsoup.connect(url);
		
		connexion.timeout(30 * 1000);
		
		final Document document = connexion.get();
		
		final Elements tables = document.getElementsByTag("table");
		Element table = null;
		final RaceParser raceParser = new RaceParser();
		
		final int nombreTable = tables.size();
		if (nombreTable > 0) {
			table = tables.first();
			final Elements linesOfTable = table.getElementsByTag("td");
			raceParser.parseRace(linesOfTable, saison, gp);
		}
		final Race race = raceParser.extractData();
		return race;
	}

	
	public void setScanner(final Scanner scanner) {
		this.scanner = scanner;
	}
}