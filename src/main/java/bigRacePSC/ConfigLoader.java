package bigRacePSC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigLoader {
	
	public static final String PARAM_NAME_DIRECTORY_PATH = "championship_directory";
	public static final String PARAM_NAME_PARTICIPANTS_FILE_PATH = "participants_pathfile";
	
	private final String configFile = "/gpro_config.txt";
	private Map<String, String> parameters;
	
	
	public ConfigLoader() {
		parameters = new HashMap<String, String>();
	}
	
	
	public void readConfigFile() throws Exception {
		BufferedReader br = null;
		String ligne = null;
		
		String filePath = new File("").getAbsolutePath();
		
		try {
			br = new BufferedReader(new FileReader(filePath + this.configFile));
			ligne = br.readLine();
			while(ligne != null) {
				manageConfigFileLineContent(ligne);
				ligne = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
	}
	
	
	public void manageConfigFileLineContent(final String ligne) throws Exception {
		final String[] splittedLine = ligne.split("=");
		
		if (splittedLine != null && splittedLine.length == 2) {
			
			String nomParametre = "";
			String valeurParametre = "";
			
			if (splittedLine[0] != null) {
				nomParametre = splittedLine[0].trim();
			}
			
			if (splittedLine[1] != null) {
				valeurParametre = splittedLine[1].trim();
			}
			addEntry(nomParametre, valeurParametre);
			
		} else {
			throw new Exception ("Le fichier ne configuration n'a pas pu être lu correctement!\n"
					+ "La ligne qui pose problème est la suivante : " + ligne);
		}
	}
	
	
	private void addEntry(final String key, final String value) {
		parameters.put(key, value);
	}

	
	public Map<String, String> getParameters() {
		return parameters;
	}
	
	
	public List<Manager> getManagers(final String fileName) throws Exception {
		
		final List<Manager> result = new ArrayList<>();
		
		BufferedReader br = null;
		String ligne = null;
		
		try {
			br = new BufferedReader(new FileReader(fileName));
			ligne = br.readLine();
			while(ligne != null) {
				final Manager currentManager = manageManagerFileLineContent(ligne);
				result.add(currentManager);
				ligne = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
		
		return result;
	}
	
	
	private Manager manageManagerFileLineContent(final String line) throws Exception {
		
		final Manager result = new Manager();
		
		if (line == null) {
			return null;
		}
		
		final String[] splittedLine = line.split(";");
		
		if (splittedLine != null && splittedLine.length >= 2) {
			
			Integer id = 0;
			String name = "";
			String team = "";
			String group = "";
			
			if (splittedLine[0] != null) {
				id = Integer.valueOf(splittedLine[0].trim());
			} else {
				throw new Exception ("Le fichier des managers n'a pas pu être lu correctement : ID absent.");
			}
			
			if (splittedLine[1] != null) {
				name = splittedLine[1].trim();
			} else {
				throw new Exception ("Le fichier des managers n'a pas pu être lu correctement : NOM absent.");
			}
			
			if (splittedLine[2] != null) {
				team = splittedLine[2].trim();
			} // un manager peut ne pas avoir d'équipe : on ne lève pas d'exception si null
			
			if (splittedLine[3] != null) {
				group = splittedLine[3].trim();
			} else {
				throw new Exception ("Le fichier des managers n'a pu être lu correctement : GROUPE absent.");
			}
			
			result.setId(id);
			result.setName(name);
			result.setTeam(team);
			result.setGroup(group);
			result.calculateDivision();
			
		} else {
			throw new Exception ("Le fichier des managers n'a pas pu être lu correctement!\n"
					+ "La ligne qui pose problème est la suivante : " + line);
		}
		
		return result;
	}
}
