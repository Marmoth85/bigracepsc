package bigRacePSC;

import java.util.Comparator;

public class DivisionResult {
	
	private Integer gp = null;
	private Integer position = null;
	private Manager manager = null;
	private RaceTime raceTime = null;
	private String classification = null;
	private RaceTime bestLap = null;
	private Integer points = null;

	
	public DivisionResult(final Manager man, final Race race, final int gp) throws Exception {
		this.gp = gp;
		manager = man;
		classification = race.getPositionFinale();
		raceTime = new RaceTime();
		raceTime.setStringTime(race.getTempsCourse());
		raceTime.calculateNumericTimeFromString(true);
		bestLap = new RaceTime();
		bestLap.setStringTime(race.getRecordTour());
		bestLap.calculateNumericTimeFromString(false);
	}
	
	
	public static Comparator<DivisionResult> DIVISION_GP_TIME_COMPARATOR = new Comparator<DivisionResult>() {
		
		@Override
		public int compare(DivisionResult o1, DivisionResult o2) {
			try {
				if (o1.getManager().getDivision() < o2.getManager().getDivision()) {
					return -1;
				} else if (o1.getManager().getDivision() > o2.getManager().getDivision()) {
					return 1;
				} else {
					if (o1.getRaceTime().getNumericTime() < o2.getRaceTime().getNumericTime()) {
						return -1;
					} else if (o1.getRaceTime().getNumericTime() > o2.getRaceTime().getNumericTime()) {
						return 1;
					} else {
						return 0;
					}
				}
			} catch (NullPointerException e) {
				System.err.println("ERREUR : Null Pointer sur la comparaison de temps sur deux DivisionResult :\n" + e.getMessage());
				return 0;
			}
		}
	};
	
	
	public static Comparator<DivisionResult> DIVISION_GP_ID_COMPARATOR = new Comparator<DivisionResult>() {
		
		@Override
		public int compare(DivisionResult o1, DivisionResult o2) {
			try {
				return o1.getManager().getId() - o2.getManager().getId();
			} catch (NullPointerException e) {
				System.err.println("ERREUR : Null Pointer sur la comparaison de ID sur deux DivisionResult :\n" + e.getMessage());
				return 0;
			}
		}
	};
	
	
	public Integer getGp() {
		return gp;
	}


	public void setGp(Integer gp) {
		this.gp = gp;
	}


	public Integer getPosition() {
		return position;
	}


	public void setPosition(Integer position) {
		this.position = position;
	}


	public Manager getManager() {
		return manager;
	}


	public void setManager(Manager manager) {
		this.manager = manager;
	}


	public RaceTime getRaceTime() {
		return raceTime;
	}


	public void setRaceTime(RaceTime raceTime) {
		this.raceTime = raceTime;
	}


	public String getClassification() {
		return classification;
	}


	public void setClassification(String classification) {
		this.classification = classification;
	}


	public RaceTime getBestLap() {
		return bestLap;
	}


	public void setBestLap(RaceTime bestLap) {
		this.bestLap = bestLap;
	}


	public Integer getPoints() {
		return points;
	}


	public void setPoints(Integer points) {
		this.points = points;
	}


	@Override
	public String toString() {
		return "DivisionResult [gp=" + gp + ", position=" + position + ", manager=" + manager + ", raceTime=" + raceTime
				+ ", classification=" + classification + ", bestLap=" + bestLap + ", points=" + points + "]";
	}
}
