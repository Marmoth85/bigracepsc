package bigRacePSC;

public class Race {
	
	private Integer saison = null;
	private Integer course = null;
	private String circuit = null;
	private String groupe = null;
	private Integer toursFinis = null;
	private Integer toursMax = null;
	private String qualif1 = null;
	private String positionQualif1 = null;
	private String qualif2 = null;
	private String positionQualif2 = null;
	private String positionFinale = null;
	private String tempsCourse = null;
	private String recordTour = null;
	private String positionRecordTour = null;
	private String pneus = null;
	private Integer nombreArrets = null;
	private String tempsArretMin = null;
	private Integer toursAvecProblemeTechnnique = null;
	
	
	public Race() {
		super();
	}

	
	public Integer getSaison() {
		return saison;
	}

	
	public void setSaison(Integer saison) {
		this.saison = saison;
	}

	
	public Integer getCourse() {
		return course;
	}

	
	public void setCourse(Integer course) {
		this.course = course;
	}

	
	public String getCircuit() {
		return circuit;
	}

	
	public void setCircuit(String circuit) {
		this.circuit = circuit;
	}

	
	public String getGroupe() {
		return groupe;
	}

	
	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

	
	public Integer getToursFinis() {
		return toursFinis;
	}

	
	public void setToursFinis(Integer toursFinis) {
		this.toursFinis = toursFinis;
	}

	
	public Integer getToursMax() {
		return toursMax;
	}

	
	public void setToursMax(Integer toursMax) {
		this.toursMax = toursMax;
	}

	
	public String getQualif1() {
		return qualif1;
	}

	
	public void setQualif1(String qualif1) {
		this.qualif1 = qualif1;
	}

	
	public String getPositionQualif1() {
		return positionQualif1;
	}

	
	public void setPositionQualif1(String positionQualif1) {
		this.positionQualif1 = positionQualif1;
	}

	
	public String getQualif2() {
		return qualif2;
	}

	
	public void setQualif2(String qualif2) {
		this.qualif2 = qualif2;
	}

	
	public String getPositionQualif2() {
		return positionQualif2;
	}

	
	public void setPositionQualif2(String positionQualif2) {
		this.positionQualif2 = positionQualif2;
	}

	
	public String getPositionFinale() {
		return positionFinale;
	}

	
	public void setPositionFinale(String positionFinale) {
		this.positionFinale = positionFinale;
	}

	
	public String getTempsCourse() {
		return tempsCourse;
	}

	
	public void setTempsCourse(String tempsCourse) {
		this.tempsCourse = tempsCourse;
	}

	
	public String getRecordTour() {
		return recordTour;
	}

	
	public void setRecordTour(String recordTour) {
		this.recordTour = recordTour;
	}

	
	public String getPositionRecordTour() {
		return positionRecordTour;
	}

	
	public void setPositionRecordTour(String positionRecordTour) {
		this.positionRecordTour = positionRecordTour;
	}

	
	public String getPneus() {
		return pneus;
	}

	
	public void setPneus(String pneus) {
		this.pneus = pneus;
	}

	
	public Integer getNombreArrets() {
		return nombreArrets;
	}

	
	public void setNombreArrets(Integer nombreArrets) {
		this.nombreArrets = nombreArrets;
	}

	
	public String getTempsArretMin() {
		return tempsArretMin;
	}

	
	public void setTempsArretMin(String tempsArretMin) {
		this.tempsArretMin = tempsArretMin;
	}

	
	public Integer getToursAvecProblemeTechnnique() {
		return toursAvecProblemeTechnnique;
	}

	
	public void setToursAvecProblemeTechnnique(Integer toursAvecProblemeTechnnique) {
		this.toursAvecProblemeTechnnique = toursAvecProblemeTechnnique;
	}

	
	@Override
	public String toString() {
		return "Race [saison=" + saison + ", course=" + course + ", circuit=" + circuit + ", groupe=" + groupe
				+ ", toursFinis=" + toursFinis + ", toursMax=" + toursMax + ", qualif1=" + qualif1
				+ ", positionQualif1=" + positionQualif1 + ", qualif2=" + qualif2 + ", positionQualif2="
				+ positionQualif2 + ", positionFinale=" + positionFinale + ", tempsCourse=" + tempsCourse
				+ ", recordTour=" + recordTour + ", positionRecordTour=" + positionRecordTour + ", pneus=" + pneus
				+ ", nombreArrets=" + nombreArrets + ", tempsArretMin=" + tempsArretMin
				+ ", toursAvecProblemeTechnnique=" + toursAvecProblemeTechnnique + "]";
	}
}