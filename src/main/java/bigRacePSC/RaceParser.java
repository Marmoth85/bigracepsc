package bigRacePSC;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class RaceParser {

	private String saison = null;
	private String course = null;
	private String circuit = null;
	private String groupe = null;
	private String toursFaits = null;
	private String qualif1 = null;
	private String qualif2 = null;
	private String grilleDepart = null;
	private String positionFinale = null;
	private String tempsCourse = null;

	private String pneus = null;
	private String pits = null;
	private String humidite = null;
	private String toursPluie = null;
	private String temperature = null;
	private String tourProblemeTechnique = null;
	private String erreur = null;
	private String vitesse = null;
	private String recordTour = null;
	private String tempsArretMin = null;

	
	public RaceParser() {
		super();
	}
	
	
	public Race extractData() {

		final Race race = new Race();
		if (StringUtils.isNotBlank(saison)) {
			race.setSaison(Integer.valueOf(saison.trim()));
		}
		if (StringUtils.isNotBlank(course)) {
			race.setCourse(Integer.valueOf(course.trim()));
		}
		if (StringUtils.isNotBlank(circuit)) {
			race.setCircuit(circuit.trim());
		}
		if (StringUtils.isNotBlank(groupe)) {
			race.setGroupe(groupe.trim());
		}
		extractToursFaits(race);
		extractQ1Info(race);
		extractQ2Info(race);
		if (StringUtils.isNotBlank(positionFinale)) {
			race.setPositionFinale(positionFinale.trim());
		} else {
			race.setPositionFinale("--");
		}
		extractTempsCourse(race);
		extractRecordTour(race);
		
		if (StringUtils.isNotBlank(pneus)) {
			race.setPneus(pneus.trim());
		}
		
		if (StringUtils.isNotBlank(pits)) {
			race.setNombreArrets(Integer.valueOf(pits.trim()));
		}
		
		if (StringUtils.isNotBlank(tempsArretMin)) {
			race.setTempsArretMin(tempsArretMin.trim());
		}
		
		if (StringUtils.isNotBlank(tourProblemeTechnique)) {
			race.setToursAvecProblemeTechnnique(Integer.valueOf(tourProblemeTechnique.trim()));
		}
		
		return race;
	}

	
	private void extractToursFaits(final Race race) {
		
		if (StringUtils.isNotBlank(toursFaits)) {

			final String[] tours = toursFaits.split("/");
			if (tours.length == 2) {
				if (StringUtils.isNotBlank(tours[0])) {
					race.setToursFinis(Integer.valueOf(tours[0].trim()));
				}
				if (StringUtils.isNotBlank(tours[1])) {
					race.setToursMax(Integer.valueOf(tours[1].trim()));
				}
			}
		}
	}
	
	
	private void extractQ1Info(final Race race) {
		
		if (StringUtils.isNotBlank(qualif1)) {
			
			final String[] tmp = qualif1.split(" ");
			if (tmp != null && tmp.length == 2) {
				if (StringUtils.isNotBlank(tmp[0])) {
					String q1Time = formalizeTimeQualif(tmp[0].trim());
					race.setQualif1(q1Time);
				}
				int n = tmp[1].length();
				if (StringUtils.isNotBlank(tmp[1]) && tmp[1].length() >= 2) {
					race.setPositionQualif1(tmp[1].substring(1, n - 1));
				}
			}
		}
	}
	
	
	private void extractQ2Info(final Race race) {
		
		if (StringUtils.isNotBlank(qualif2)) {
			
			final String[] tmp = qualif2.split(" ");
			if (tmp != null && tmp.length == 2) {
				if (StringUtils.isNotBlank(tmp[0])) {
					String q2Time = formalizeTimeQualif(tmp[0].trim());
					race.setQualif2(q2Time);
				}
				int n = tmp[1].length();
				if (StringUtils.isNotBlank(tmp[1]) && tmp[1].length() >= 2) {
					race.setPositionQualif2(tmp[1].substring(1, n - 1));
				}
			}
		}
	}
	
	
	private void extractTempsCourse(final Race race) {
		
		if (StringUtils.isNotBlank(tempsCourse)) {
			
			final String[] tmp = tempsCourse.split(" ");
			if (tmp != null && tmp.length > 1) {
				String val = tmp[0];
				if (StringUtils.isNotBlank(val)) {
					val = formalizeTime(val.trim());
					race.setTempsCourse(val);
				}
			} else if (tmp != null && tmp.length == 1) {
				if (StringUtils.isNotBlank(tmp[0])) {
					race.setTempsCourse(tmp[0].trim());
				}
			} else {
				System.err.println("Erreur de parsing sur le temps de course");
			}
		}
	}
	
	
	private void extractRecordTour(final Race race) {
		
		if (StringUtils.isNotBlank(recordTour)) {
			
			final String[] tmp = recordTour.split(" ");
			if (tmp != null && tmp.length > 1) {
				if (StringUtils.isNotBlank(tmp[0])) {
					final String mt = formalizeTimeQualif(tmp[0].trim());
					race.setRecordTour(mt);
				}
				int n = tmp[1].length();
				if (StringUtils.isNotBlank(tmp[1]) && tmp[1].length() >= 3) {
					race.setPositionRecordTour(tmp[1].substring(2, n - 1));
				}
			} else {
				race.setRecordTour("--");
			}
		} else {
			race.setRecordTour("--");
		}
	}
	
	
	public void cleanAllRecords() {
		saison = null;
		course = null;
		circuit = null;
		groupe = null;
		toursFaits = null;
		qualif1 = null;
		qualif2 = null;
		grilleDepart = null;
		positionFinale = null;
		tempsCourse = null;
		pneus = null;
		pits = null;
		humidite = null;
		toursPluie = null;
		temperature = null;
		tourProblemeTechnique = null;
		erreur = null;
		vitesse = null;
		recordTour = null;
		tempsArretMin = null;
	}
	
	
	public String formalizeTime(final String time) {
		String result = "";
		
		if (StringUtils.isNotBlank(time)) {
			String[] tmp = time.split("h");
			String minutes = "";
			if (tmp.length >= 2) {
				result += tmp[0] + "h";
				for (int i = 1; i < tmp.length; i++) {
					minutes += tmp[i];
				}
			}
			
			String seconds = "";
			if (StringUtils.isNotBlank(minutes)) {
				tmp = minutes.split(":");
				if (tmp.length >= 2) {
					final Integer min = Integer.valueOf(tmp[0]);
					for (int i = 1; i < tmp.length; i++) {
						seconds += tmp[i];
					}
					if (min < 10) {
						result += "0" + min + ":";
					} else {
						result += min + ":";
					}
				}
			}
			
			if (StringUtils.isNotBlank(seconds)) {
				result += seconds;
			}
		}
		
		return result;
	}
	
	
	public String formalizeTimeQualif(final String time) {
		String result = "";
		
		if (StringUtils.isNotBlank(time)) {
			String[] tmp = time.split(":");
			String seconds = "";
			if (tmp.length >= 2) {
				final Integer min = Integer.valueOf(tmp[0]);
				for (int i = 1; i < tmp.length; i++) {
					seconds += tmp[i];
				}
				result += min + ":";
			} else {
				seconds = time;
			}
			
			if (StringUtils.isNotBlank(seconds)) {
				tmp = seconds.split("s");
				result += tmp[0];
			}
		}
		
		return result;
	}
	
	
	public void parseRace(final Elements tableLines, final int findSaison, final int findGP) {
		
		// L'indice i sert à itérer sur les 20 attributs relatifs aux courses
		int i = -1;
		
		// Une fois la course recherchée chargée, le booléen vaudra vrai.
		// On s'en servira pour arrêter les recherches dans le code HTML.
		boolean raceLoaded = false;

		for (final Element elmt : tableLines) {
			
			final Attributes attr = elmt.attributes();
			final String attributString = attr.get("style");
			
			// On cherche à déterminer si on a trouvé la première balise <td>
			// correspondant à la table résumant une course.
			// Si oui, on initialise i à 1 pour dire qu'on est sur le premier attribut.
			if ("border-left: 1px solid #1b2d47;".equals(attributString) && i != 10 && i != 11 && i != 20) {
				i = 1;
				cleanAllRecords();
			}

			if (i == 21 && raceLoaded) {
				break;
			}

			switch (i) {
			case 1:
				saison = elmt.text();
				break;
			case 2:
				course = elmt.text();
				if (String.valueOf(findSaison).equals(saison) && String.valueOf(findGP).equals(course)) {
					raceLoaded = true;
				}
				// DNS
				if (Integer.valueOf(saison) == findSaison && Integer.valueOf(course) < findGP || Integer.valueOf(saison) < findSaison) {
					cleanAllRecords();
					tempsCourse = "4h00:00.000";
					return;
				}
				break;
			case 3:
				circuit = elmt.text();
				break;
			case 4:
				groupe = elmt.text();
				break;
			case 5:
				toursFaits = elmt.text();
				break;
			case 6:
				qualif1 = elmt.text();
				break;
			case 7:
				qualif2 = elmt.text();
				break;
			case 8:
				grilleDepart = elmt.text();
				break;
			case 9:
				positionFinale = elmt.text();
				break;
			case 10:
				tempsCourse = elmt.text();
				break;
			case 11:
				pneus = elmt.getElementsByAttribute("title").attr("title");
				break;
			case 12:
				pits = elmt.text();
				break;
			case 13:
				humidite = elmt.text();
				break;
			case 14:
				toursPluie = elmt.text();
				break;
			case 15:
				temperature = elmt.text();
				break;
			case 16:
				tourProblemeTechnique = elmt.text();
				break;
			case 17:
				erreur = elmt.text();
				break;
			case 18:
				vitesse = elmt.text();
				break;
			case 19:
				recordTour = elmt.text();
				break;
			case 20:
				tempsArretMin = elmt.text();
				break;
			}
			++i;
		}
		if (tableLines.isEmpty()) {
			tempsCourse = "4h00:00.000";
		}
	}

	
	public String getSaison() {
		return saison;
	}

	
	public String getCourse() {
		return course;
	}

	
	public String getCircuit() {
		return circuit;
	}

	
	public String getGroupe() {
		return groupe;
	}

	
	public String getToursFaits() {
		return toursFaits;
	}

	
	public String getQualif1() {
		return qualif1;
	}

	
	public String getQualif2() {
		return qualif2;
	}

	
	public String getGrilleDepart() {
		return grilleDepart;
	}

	
	public String getPositionFinale() {
		return positionFinale;
	}

	
	public String getTempsCourse() {
		return tempsCourse;
	}

	
	public String getPneus() {
		return pneus;
	}

	
	public String getPits() {
		return pits;
	}

	
	public String getHumidite() {
		return humidite;
	}

	
	public String getToursPluie() {
		return toursPluie;
	}

	
	public String getTemperature() {
		return temperature;
	}

	
	public String getTourProblemeTechnique() {
		return tourProblemeTechnique;
	}

	
	public String getErreur() {
		return erreur;
	}

	
	public String getVitesse() {
		return vitesse;
	}

	
	public String getRecordTour() {
		return recordTour;
	}

	
	public String getTempsArretMin() {
		return tempsArretMin;
	}

	
	@Override
	public String toString() {
		return "RaceParser [saison=" + saison + ", course=" + course + ", circuit=" + circuit + ", groupe=" + groupe
				+ ", toursFaits=" + toursFaits + ", qualif1=" + qualif1 + ", qualif2=" + qualif2 + ", grilleDepart="
				+ grilleDepart + ", positionFinale=" + positionFinale + ", tempsCourse=" + tempsCourse + ", pneus="
				+ pneus + ", pits=" + pits + ", humidite=" + humidite + ", toursPluie=" + toursPluie + ", temperature="
				+ temperature + ", tourProblemeTechnique=" + tourProblemeTechnique + ", erreur=" + erreur + ", vitesse="
				+ vitesse + ", recordTour=" + recordTour + ", tempsArretMin=" + tempsArretMin + "]";
	}
}