package bigRacePSC;

import java.util.Comparator;

public class BigRaceResult {

	private Integer position = null;
	private Manager manager = null;
	private RaceTime raceTime = null;
	private RaceTime gapPrevious = null;
	private RaceTime gapLeader = null;
	
	
	public BigRaceResult(final Manager man, final Race race) throws Exception {
		manager = man;
		raceTime = new RaceTime();
		raceTime.setStringTime(race.getTempsCourse());
		raceTime.calculateNumericTimeFromString(true);
	}
	
	
	public static Comparator<BigRaceResult> BIG_RACE_GP_TIME_COMPARATOR = new Comparator<BigRaceResult>() {

		@Override
		public int compare(BigRaceResult o1, BigRaceResult o2) {
			try {
				if (o1.getRaceTime().getNumericTime() < o2.getRaceTime().getNumericTime()) {
					return -1;
				} else if (o1.getRaceTime().getNumericTime() > o2.getRaceTime().getNumericTime()) {
					return 1;
				} else {
					return 0;
				}
			} catch (NullPointerException e) {
				System.err.println("ERREUR : Null Pointer sur la comparaison de temps sur deux BigRaceResult :\n" + e.getMessage());
				return 0;
			}
		}
	};
	
	
	public static Comparator<BigRaceResult> BIG_RACE_GP_ID_COMPARATOR = new Comparator<BigRaceResult>() {

		@Override
		public int compare(BigRaceResult o1, BigRaceResult o2) {
			try {
				if (o1.getManager().getId() < o2.getManager().getId()) {
					return -1;
				} else if (o1.getManager().getId() > o2.getManager().getId()) {
					return 1;
				} else {
					return 0;
				}
			} catch (NullPointerException e) {
				System.err.println("ERREUR : Null Pointer sur la comparaison de deux ID sur deux BigRaceResult :\n" + e.getMessage());
				return 0;
			}
		}
	};
	
	
	public Integer getPosition() {
		return position;
	}


	public void setPosition(Integer position) {
		this.position = position;
	}


	public Manager getManager() {
		return manager;
	}


	public void setManager(Manager manager) {
		this.manager = manager;
	}


	public RaceTime getRaceTime() {
		return raceTime;
	}


	public void setRaceTime(RaceTime raceTime) {
		this.raceTime = raceTime;
	}


	public RaceTime getGapPrevious() {
		return gapPrevious;
	}


	public void setGapPrevious(RaceTime gapPrevious) {
		this.gapPrevious = gapPrevious;
	}


	public RaceTime getGapLeader() {
		return gapLeader;
	}


	public void setGapLeader(RaceTime gapLeader) {
		this.gapLeader = gapLeader;
	}


	@Override
	public String toString() {
		return "BigRaceResult [position=" + position + ", manager=" + manager + ", raceTime=" + raceTime
				+ ", gapPrevious=" + gapPrevious + ", gapLeader=" + gapLeader + "]";
	}
}
