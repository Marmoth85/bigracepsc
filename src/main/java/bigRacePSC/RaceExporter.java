package bigRacePSC;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class RaceExporter {
	
	private Map<Manager, Race> lastGP;

	
	public RaceExporter() {
		// TODO Auto-generated constructor stub
	}

	
	public RaceExporter(final Map<Manager, Race> lastGP) {
		super();
		this.lastGP = lastGP;
	}
	
	
	public void createDirectoryIfNotExists(final String nomFichier) {
		final File f = new File(nomFichier);
		final File parent = f.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}
	}
	
	
	public void ecrireFichierCSV(final String nomFichier) throws IOException {
		
		createDirectoryIfNotExists(nomFichier);
		
		BufferedWriter bw = null;
		String ligne = "";
		try {
			bw = new BufferedWriter(new FileWriter(nomFichier, false));
			final Set<Map.Entry<Manager, Race>> perf = this.lastGP.entrySet();
			final Iterator<Map.Entry<Manager, Race>> iter = perf.iterator();
			ligne = "Manager ID;" +
					"Manager Name;" +
					"Manager Team;" +
					"Tps de course;"+
					"Classement;" +
					"MT;" +
					"Groupe; " +
					"Position MT;" +
					"Nb.arrêts;" +
					"Tps arrêt Min;" +
					"Pneus; " +
					"Circuit;" + 
					"Tours finis;" + 
					"Tours Max;" +
					"Tours avec pb. tech.;" +
					"Saison;" +
					"Course;" +
					"Tps Q1;" +
					"Pos. Q1;" +
					"Tps Q2;" +
					"Pos. Q2;";
			bw.write(ligne);
			bw.newLine();
			
			while (iter.hasNext()) {
				
				final Map.Entry<Manager, Race> paire = iter.next();
				final Manager m = paire.getKey();
				final Race r = paire.getValue();
				
				ligne = m.getId() + ";" +
						m.getName() + ";" +
						m.getTeam() + ";" +
						r.getTempsCourse() + ";" + 
						r.getPositionFinale() + ";" +
						r.getRecordTour() + ";" +
						r.getGroupe() + ";" +
						r.getPositionRecordTour() + ";" +
						r.getNombreArrets() + ";" +
						r.getTempsArretMin() + ";" +
						r.getPneus() + ";" +
						r.getCircuit() + ";" + 
						r.getToursFinis() + ";" + 
						r.getToursMax() + ";" +
						r.getToursAvecProblemeTechnnique() + ";" +
						r.getSaison() + ";" +
						r.getCourse() + ";" +
						r.getQualif1() + ";" +
						r.getPositionQualif1() + ";" +
						r.getQualif2() + ";" +
						r.getPositionQualif2() + ";";
				
				bw.write(ligne);
				bw.newLine();
			}
			
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}
}