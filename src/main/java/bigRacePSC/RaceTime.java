package bigRacePSC;

public class RaceTime {
	
	private String strTime;
	private Double numTime;

	
	public RaceTime() {
		strTime = "";
		numTime= 0d;
	}

	
	public void setStringTime (final String time) {
		strTime = time;
	}
	
	
	public String getStringTime() {
		return strTime;
	}
	
	
	public void setNumericTime(final Double time) {
		numTime = time;
	}
	
	
	public Double getNumericTime() {
		return numTime;
	}
	
	
	@Override
	public String toString() {
		return "RaceTime [strTime=" + strTime + ", numTime=" + numTime + "]";
	}


	public void calculateStringTimeFromNumeric() {
		
		Double time = numTime;
		Integer hours = 0;
		Integer minutes = 0;
		Double seconds = 0d;
		
		if(time == 0) {
			setStringTime("--");
			return;
		}
		
		hours = (int)(time / 3600);
		time -= hours * 3600;
		
		minutes = (int)(time / 60);
		time -= minutes * 60;
		seconds = time;
		
		String result = "";
		if (hours > 0) {
			result += hours + "h";
		}
		if (minutes > 0 || hours > 0) {
			if (minutes < 10) {
				result += "0";
			}
			result += minutes + ":";
		}
		if (seconds < 10) {
			result += "0";
		}
		double s = (double)(Math.round(seconds * 1000));
		result += s / 1000;
		if (s % 100 == 0) {
			result += "00";
		} else if (s % 10 == 0) {
			result += "0";
		}
		
		setStringTime(result);
	}
	
	
	public void calculateNumericTimeFromString(final boolean withHours) throws Exception {

		String time = strTime;
		Integer hours = 0; 
		Integer minutes = 0;
		Double seconds = 0d;
		
		if (withHours) {
			final String[] splittedStringHours = time.split("h");
			if (splittedStringHours != null && splittedStringHours.length == 2) {
				hours = Integer.valueOf(splittedStringHours[0]);
				time = splittedStringHours[1];
			} else {
				throw new Exception("Le temps de course n'est pas bien formé : " + time);
			}
		}
		
		final String[] splittedStringMinutes = time.split(":");
		if (splittedStringMinutes != null && splittedStringMinutes.length == 2) {
			minutes = Integer.valueOf(splittedStringMinutes[0]);
			seconds = Double.valueOf(splittedStringMinutes[1]);
		} else {
			// Le temps traité ne possède pas de minutes... circuit best lap < 1 mn
			if (!"--".equals(time)) {
				seconds = Double.valueOf(time);
			} else {
				setNumericTime(0d);
				return;
			}
		}
		
		final Double numericTime = 3600d * hours + 60d * minutes + seconds;
		setNumericTime(numericTime);
	}
}
