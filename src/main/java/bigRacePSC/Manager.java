package bigRacePSC;

import org.apache.commons.lang3.StringUtils;

public class Manager {
	
	private Integer id;
	private String name;
	private String team;
	private String group;
	private int division;

	
	public Manager() {
		id = 0;
		name = "";
		team = "";
		group = "";
		division = 0;
	}


	public Manager(Integer id, String name, String team, String group) {
		super();
		this.id = id;
		this.name = name;
		this.team = team;
		this.group = group;
		calculateDivision();
	}

	
	public void calculateDivision() {
		if (StringUtils.isBlank(group)) {
			division = 0;
		} else {
			if (group.equals("Elite")) {
				division = 1;
			} else if (group.contains("Master")) {
				division = 2;
			} else if (group.contains("Pro")) {
				division = 3;
			} else if (group.contains("Amateur")) {
				division = 4;
			} else if (group.contains("Rookie")) {
				division = 5;
			}
		}
	}
	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTeam() {
		return team;
	}


	public void setTeam(String team) {
		this.team = team;
	}


	public String getGroup() {
		return group;
	}


	public void setGroup(String group) {
		this.group = group;
	}


	public int getDivision() {
		return division;
	}


	public void setDivision(int division) {
		this.division = division;
	}


	@Override
	public String toString() {
		return "Manager [id=" + id + ", name=" + name + ", team=" + team + ", group=" + group + ", division=" + division
				+ "]";
	}
}