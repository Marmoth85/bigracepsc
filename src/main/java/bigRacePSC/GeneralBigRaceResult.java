package bigRacePSC;

import java.util.Comparator;

public class GeneralBigRaceResult {
	
	private Integer position = null;
	private Manager manager = null;
	private RaceTime lastRace = null;
	private RaceTime globalTime = null;
	private RaceTime gapPrevious = null;
	private RaceTime gapLeader = null;

	
	public GeneralBigRaceResult(final Manager m, final Race race, final RaceTime g) throws Exception {
		
		if (g == null) {
			globalTime = new RaceTime();
			globalTime.setNumericTime(0d);
		} else {
			globalTime = g;
		}
		
		manager = m;
		lastRace = new RaceTime();
		lastRace.setStringTime(race.getTempsCourse());
		lastRace.calculateNumericTimeFromString(true);
	}
	
	
	public void addRaceTimeToGlobalTime() throws Exception {
		
		if (globalTime == null || globalTime.getNumericTime() == null) {
			throw new Exception("Le Temps globalTime est NULL!!!\n");
		}
		
		if (lastRace == null || lastRace.getNumericTime() == null) {
			throw new Exception("Le temps de course est NULL!!!\n");
		}
		
		globalTime.setNumericTime(globalTime.getNumericTime() + lastRace.getNumericTime());
		globalTime.calculateStringTimeFromNumeric();
	}
	
	
	public static Comparator<GeneralBigRaceResult> BIG_RACE_GENERAL_TIME_COMPARATOR = new Comparator<GeneralBigRaceResult>() {

		@Override
		public int compare(GeneralBigRaceResult o1, GeneralBigRaceResult o2) {
			try {
				if (o1.getGlobalTime().getNumericTime() < o2.getGlobalTime().getNumericTime()) {
					return -1;
				} else if (o1.getGlobalTime().getNumericTime() > o2.getGlobalTime().getNumericTime()) {
					return 1;
				} else {
					return 0;
				}
			} catch (NullPointerException e) {
				System.err.println("ERREUR : Null pointer sur la comparaison de temps entre deux GeneralBigRaceResult :\n" + e.getMessage());
				return 0;
			}
		}
	};
	
	
	public static Comparator<GeneralBigRaceResult> BIG_RACE_GENERAL_ID_COMPARATOR = new Comparator<GeneralBigRaceResult>() {

		@Override
		public int compare(GeneralBigRaceResult o1, GeneralBigRaceResult o2) {
			try {
				return o1.getManager().getId() - o2.getManager().getId();
			} catch (NullPointerException e) {
				System.err.println("ERREUR : Null Pointer sur la comparaison de deux ID de GeneralBigRaceResult :\n" + e.getMessage());
				return 0;
			}
		}
		
	};


	public Integer getPosition() {
		return position;
	}


	public void setPosition(Integer position) {
		this.position = position;
	}


	public Manager getManager() {
		return manager;
	}


	public void setManager(Manager manager) {
		this.manager = manager;
	}


	public RaceTime getLastRace() {
		return lastRace;
	}


	public void setLastRace(RaceTime lastRace) {
		this.lastRace = lastRace;
	}


	public RaceTime getGlobalTime() {
		return globalTime;
	}


	public void setGlobalTime(RaceTime globalTime) {
		this.globalTime = globalTime;
	}


	public RaceTime getGapPrevious() {
		return gapPrevious;
	}


	public void setGapPrevious(RaceTime gapPrevious) {
		this.gapPrevious = gapPrevious;
	}


	public RaceTime getGapLeader() {
		return gapLeader;
	}


	public void setGapLeader(RaceTime gapLeader) {
		this.gapLeader = gapLeader;
	}


	@Override
	public String toString() {
		return "GeneralBigRaceResult [position=" + position + ", manager=" + manager + ", lastRace=" + lastRace
				+ ", globalTime=" + globalTime + ", gapPrevious=" + gapPrevious + ", gapLeader=" + gapLeader + "]";
	}
}
